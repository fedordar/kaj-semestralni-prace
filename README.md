# Json data visualization
###### React application for KAJ semestral project ######

## Application
In the project directory run:
```npm start```
It will run the app in the development mode and open http://localhost:3000 to view it in the browser.
The website consists of 4 pages:
- the introductory page contains a description and a sequence of subsequent actions. 
- the page with filling in the data. For each data set, you need to create a so-called scene. Fill in the name and description of the scene in the form. Next, provide the json data via url, a file, or simply paste it into text field. For testing, test data is provided, which will be fulfilled when you click on the "Fill the form with testing data" button.
- then there will be a preview page of your data as well as the configuration of tables and diagrams. There are three types in total: ordinary table, line chart and pie chart. Choose which one you require and configure it. 
- the last page is the result on which you will see the visualization of your data in the form in which you have chosen.

## Technologies ##
#### HTML ####
- html5 semantic tags
- form elements (validation, type, placeholder, autofocus)
- SVG

#### CSS ####
- Use of advanced pseudo-classes and combinators
- vendor prefixes
- css2/3 transformation,
- animation, transitions
- media queries

#### JS ####
- OOP
- React framework
- local memory, Drag & Drop API
- History Api (useNavigate)
