import React from 'react';
import Plot from 'react-plotly.js';
import Scene from "./components/Scene/Scene";
import {HashRouter, Route, Routes} from "react-router-dom";
import Api, {LineChart} from "./components/Charts/LineChart";
import {PreviewPage} from "./components/Preview/PreviewPage";
import {TablePage} from "./components/Table/TablePage";
import {PieChart} from "./components/Charts/PieChart";
import Main from "./components/Main";
import {ScenePage} from "./components/Scene/ScenePage";

const App = () => {
    return (
            <HashRouter basename={process.env.PUBLIC_URL}>
                <Routes>
                    <Route path="/" element ={<Main />}>
                    </Route>
                    <Route path="/scene" element ={<ScenePage />}>
                    </Route>
                    <Route path="/Api" element ={<LineChart />}>
                    </Route>
                    <Route path="/preview" element ={<PreviewPage />}>
                    </Route>
                    <Route path="/table" element={<TablePage />} />
                    <Route path="/lineChart" element={<LineChart />} />
                    <Route path="/pieChart" element={<PieChart />} />
                </Routes>
            </HashRouter>
    );
};
export default App;