import React from 'react';
import { ReactComponent as Data }  from '../data-analytics.svg';
import { ReactComponent as Json }  from '../json.svg';
import { ReactComponent as Chart }  from '../chart.svg';
import {useNavigate} from "react-router-dom"

const Main = () => {
    const navigate = useNavigate()
    // presmeruje na stranku "scene"
    const toScenePage = () => {
        navigate("/scene")
    }
    return (
        <div className="main">
        <main>
            <div>Welcome to the online JSON Visualization</div>
            <section id="description-main">
                <article className="main-col-50" id="description">
                    <h1>Create charts from most popular data format in the world!</h1>
                        <p>JSON is a data format that is gaining popularity and used extensively in many AJAX-powered Web sites benefits of being it's human-readable.</p>
                        <p>It is easy to parse JSON data and generate graph accordingly. This way you can separate the UI from Data.</p>
                        <p>This website allows you to easily create beautiful charts from raw JSON data in a few simple steps.</p>
                </article>
                <article className="main-col-50">
                    <div className="main-steps-box">
                        <div className="icon"><Json className="icons-main"/></div>

                        <h2 className="title">1. Process JSON data</h2>
                        <p className="description"> Provide your JSON data by URL, file or just paste it. This website
                            will process your JSON data in a user-friendly way. </p>
                    </div>
                    <div className="main-steps-box">
                        <div className="icon"><Chart className="icons-main" /></div>
                        <h2 className="title">2. Create beautiful charts</h2>
                        <p className="description">
                            Once the data is ready, you can easily represent the values on multiple types of charts.</p>
                    </div>
                    <div className="main-steps-box">
                        <div className="icon"><Data className="icons-main" /></div>
                        <h2 className="title">3. Use the chart</h2>
                        <p className="description">
                            When the chart is ready, just use it as you want. Features like saving the chart
                            might be coming later.</p>
                    </div>
                </article>
            </section>
            <div className="start-button">
                <h2>Click to start</h2>
                <button className="start" onClick={toScenePage}>Start</button>
            </div>
        </main>
        <footer id="footer">
            <div>
                <div className="credits">Designed by Fedorova Daria for KAJ semestral project</div>
            </div>
        </footer>
        </div>
    );
}
export default Main;