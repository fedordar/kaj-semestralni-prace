import {useLocation} from "react-router-dom"
import React from "react";
import {useNavigate} from "react-router-dom";
import Scene from "./Scene";

export const ScenePage = () => {
    const navigation = useNavigate();
    return (
        <Scene navigation={navigation}/>
    )
}