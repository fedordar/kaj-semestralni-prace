import React, {Component} from 'react';
import DataSource from "./DataSource";
import localforage from "localforage";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { useHistory } from "react-router-dom";

export default class Preview extends Component{
    constructor(props) {
        super(props);
        this.state = {
            fileName : "",
            option : "Paste",
            name :"",
            description:""
        };
        this.onFormSubmit.bind(this)
    }

    // vytvori notifikace podle stavu json, pokud validni bude hlaska o uspesne pridani jsonu, jinak chybova hlaska
    createNotification (message, typ){
        if(typ === "invalid")
            return NotificationManager.error(message, 'Error!', 3000);
        return NotificationManager.success(message, 'Success!', 3000);
    }

    // z url, ktere napsal uzivatel, bude nastena json data
    async fetchJsonFromUrl(url) {
        try{
            let response = await fetch(url);
            return await response.json();
        }catch(err){
            console.error(url+" does not contains valid json");
            return null
        }
    }

    // ze souboru bude nastena json data
    async fetchJsonFromFile() {
        let file = await localforage.getItem(this.state.fileName);
        if(file == null || typeof file == "undefined")
            return ;
        let reader = new FileReader();

        return new Promise((resolve, reject) => {
            reader.onerror = () => {
                reader.abort();
                reject(new DOMException("Problem parsing input file."));
            };

            reader.onload = () => {
                resolve(reader.result);
            };
            reader.readAsText(file);
        });

    }

    //event listener na odeslani formalure. nacte json, zvaliduje a odesle dal
    onFormSubmit = async (event) => {
        event.preventDefault();
        this.state.name = document.getElementById("scene-name").value;
        this.state.description = document.getElementById("scene-desc").value;
        let json =  await this.validate(this.state.option);
        this.props.navigation("/preview",{state:{scene:this.state.name,description:this.state.description,json:json}})
    }
    // pri nahravani souboru zmeni nazev aktualniho souboru
    changedFileName = (file,selectOption) => {
      if(file!==null)
          this.state.fileName = file;
      this.state.option = selectOption;
      if(file !== null)
        this.validate(this.state.option)
    }

    // funkce validace
    validate = async (selectOption) => {
        let parsedJson;

        switch (selectOption) {
            case "Paste":
                const jsonPaste = document.getElementById("json-paste").value;
                parsedJson = this.isValidJson(jsonPaste)
                break
            case "URL":
                const url = document.getElementById("url-type-styled-input").value;
                parsedJson = await this.fetchJsonFromUrl(url)
                break;
            case "File":
                let jsonFile = await this.fetchJsonFromFile()
                parsedJson = this.isValidJson(jsonFile)
                break;
            default:
                return ;
        }
        if (parsedJson == null) {
            this.createNotification('JSON is not valid', "invalid");
            return parsedJson;
        }
        this.createNotification('JSON is valid', "valid");
        return parsedJson;
    }
    //jestli json je validni
    isValidJson = (json) => {
        try {
            return JSON.parse(json);
        } catch (e) {
            return null;
        }
    }
    //vyplnit formulář testovacimi daty
    fillForm = () =>{
        document.getElementById("scene-name").value = "My new testing name";
        document.getElementById("scene-desc").value = "My new testing description";
        document.getElementById("loadData").value = "Paste"
        document.getElementById("json-paste").value = "[{\"id\":1,\"value\":2,\"name\":\"John\",\"salary\": 3700},{\"id\":2,\"value\":1,\"name\":\"Sally\",\"salary\":2900},{\"id\":3,\"value\":5,\"name\":\"Nick\",\"salary\":1800},{\"id\":4,\"value\":7,\"name\":\"Anna\",\"salary\":4800}]"
    }
    render() {
        return (
            <div className="main">
                <main>
                    <div>Import data to scene</div>
                    <div className="description"><h2>Manage your data by creating a new scene. Just type name and
                        description. </h2>
                        <h2>Paste your data, choose a file or provide URL where JSON data is stored.</h2>
                        <button onClick={this.fillForm} id="testing">Fill the form with testing data</button>
                    </div>
                    <form onSubmit={this.onFormSubmit}>
                        <label htmlFor="scene-name">Enter a scene name:</label>
                        <input id="scene-name" name="scene-name" required autoFocus type="text"
                               placeholder="My new scene name"/>
                        <label htmlFor="scene-desc">Enter a scene description:</label>
                        <input id="scene-desc" name="scene-desc" type="text" placeholder="My new scene description"/>
                        <DataSource onSubmitFile={this.changedFileName} onChangeValidate={this.validate}/>
                        <div className="form-submit"><button >Submit</button></div>

                    </form>
                    <NotificationContainer/>
                </main>
                <footer id="footer">
                    <div>
                        <div className="credits">Designed by Fedorova Daria for KAJ semestral project</div>
                    </div>
                </footer>
            </div>
        );
    }
}

