import React, {Component} from 'react';
import {FileUploader} from "react-drag-drop-files";

const fileTypes = ["JSON"];

export default class DragDrop extends Component {
    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(file){
        this.props.fileHandler(file)
    }
    render() {
        return (
            <div style={{display:'none' }} id={"DragDrop"}>
                <FileUploader handleChange={this.handleChange} name="file" id="file" types={fileTypes} />
            </div>
        )


    }
}