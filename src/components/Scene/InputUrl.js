import React, { Component } from "react";
import '../../css/Scene.css';

class InputUrl extends Component {
    constructor(props) {
        super(props)
        this.state = {
            typingTimer : null,
            doneTypingInterval : 2000,
            myInput : null
        }
        this.addEventListener = this.addEventListener.bind(this)
        this.onUrlChange = this.onUrlChange.bind(this)
    }
    addEventListener(){
        this.state.myInput.addEventListener('keyup', () => {
            clearTimeout(this.state.typingTimer);
            if (this.state.myInput.value) {
                this.state.typingTimer = setTimeout(this.onUrlChange, this.state.doneTypingInterval);
            }
        });
    }
    componentDidMount(){
        this.state.myInput = document.getElementById('url-type-styled-input')
        this.addEventListener();
    }
    onUrlChange() {
        this.props.onChangeUrl("URL");
    }
    render() {
        return (
            <div style={{display:'none' }} id={"URL"}>
                <input type="url" placeholder="https://your-json.data/" id="url-type-styled-input"/>
            </div>
        );
    }
}
export default InputUrl;