import React from 'react';
import DragDrop from "./DragDrop";
import localforage from "localforage";
import InputUrl from "./InputUrl";
import $ from 'jquery';

// komponent pro urceni zdroje json dat
class DataSource extends React.Component {
    constructor(props) {
        super(props);
        this.setFile = this.setFile.bind(this)
        this.changeForm = this.changeForm.bind(this)

        this.state = {
            fileNameTemplate: "test",
            typingTimer: null,
            doneTypingInterval: 1000,
            myInput: null
        };
    }

    setFile = (file) => {
        let currentDate = new Date().getTime();
        let fileName = this.state.fileNameTemplate + currentDate

        let select = document.querySelector("#loadData")
        let value = select.value

        this.props.onSubmitFile(fileName, value)
        localforage.setItem(fileName, file);
        // this.validateJson(value)

    }
    validateJson = (value) => {
        this.props.onChangeValidate(value)
    }
    validateJsonText = () => {
        this.validateJson("Paste")
    }

    changeForm() {
        let select = document.querySelector("#loadData")
        let value = select.value

        $("#json-paste").hide();
        $("#upload-file-content").hide();
        $("#DragDrop").hide();
        var elms = document.getElementsByName("file")
        for (var i = 0; i < elms.length; i++) {
            elms[i].value = ""
        }
        $("#URL").hide();

        this.props.onSubmitFile(null, value)

        if (value === 'Paste') {
            $("#json-paste").show();
        } else if (value === 'File') {
            $("#DragDrop").show();
        } else if (value === 'URL') {
            $("#URL").show();
        }
    };

    componentDidMount() {
        this.state.myInput = document.getElementById('json-paste')
        this.state.myInput.addEventListener('keyup', () => {
            clearTimeout(this.state.typingTimer);
            if (this.state.myInput.value) {
                this.state.typingTimer = setTimeout(this.validateJsonText, this.state.doneTypingInterval);
            }
        })
    }

    render() {
        return (
            <div id="uploadData">
                <div className="selectdiv">
                    <label>
                        Provide JSON data
                    </label>
                    <select onChange={this.changeForm} id="loadData">
                        <option value="Paste">Paste data</option>
                        <option value="File">Choose a file</option>
                        <option value="URL">Load URL</option>
                    </select>
                </div>
                <br/>
                <DragDrop fileHandler={this.setFile}/>
                <InputUrl onChangeUrl={this.validateJson}/>
                <textarea  id={"json-paste"} placeholder="Paste or type your JSON data"/>
            </div>
        );
    }
}

export default DataSource;