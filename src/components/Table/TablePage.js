import {useLocation} from "react-router-dom"
import React from "react";
import {useNavigate} from "react-router-dom";
import Table from "./Table";

export const TablePage = () => {
    const location = useLocation();
    const ooo = useNavigate();
    return (
        <Table navigation={ooo} columnsHead={location.state.columnsHead} properties={location.state.properties}/>
    )
}