import React, {Component} from 'react';
import TrTable from "../Preview/TrTable";

export default class Table extends Component {

    constructor(props) {
        super(props)
        this.state = {
            columnsHead : this.props.columnsHead.map(item => <th key={item}> {item} </th>)
        }
    }
    download_table_as_csv() {
        const separator = ',';
        var rows = document.querySelectorAll('table#table_chart tr');
        var csv = [];
        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll('td, th');
            for (var j = 0; j < cols.length; j++) {
                // Clean innertext to remove multiple spaces and jumpline (break csv)
                var data = cols[j].innerText.replace(/(\r\n|\n|\r)/gm, '').replace(/(\s\s)/gm, ' ')
                // Escape double-quote with double-double-quote (see https://stackoverflow.com/questions/17808511/properly-escape-a-double-quote-in-csv)
                data = data.replace(/"/g, '""');
                // Push escaped string
                row.push('"' + data + '"');
            }
            csv.push(row.join(separator));
        }
        var csv_string = csv.join('\n');
        var filename = 'table_chart_from_json' + '_' + new Date().toLocaleDateString() + '.csv';
        var link = document.createElement('a');
        link.style.display = 'none';
        link.setAttribute('target', '_blank');
        link.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv_string));
        link.setAttribute('download', filename);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    render() {
        return (
            <div className="main">
                <main>
                    <div>Table result</div>
                <table className="table-json" id="table_chart">
                    <thead>
                    <tr>
                        {this.state.columnsHead}
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.properties.map((data, i) => {
                        return (<TrTable key={i} data={this.props.properties[i]} columns={this.props.columnsHead}/> )
                    })}
                    </tbody>
                </table>
                <button id="testing" onClick={this.download_table_as_csv}>Download as CSV</button>
                </main>
                <footer id="footer">
                    <div>
                        <div className="credits">Designed by Fedorova Daria for KAJ semestral project</div>
                    </div>
                </footer>
            </div>
        )


    }
}