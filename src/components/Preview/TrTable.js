import React, {Component} from "react";
import {FileUploader} from "react-drag-drop-files";

export default class TrTable extends Component {
    render() {
        return (
            <tr>
                {this.props.columns.map( i => <td  key={i}>{this.props.data[i]}</td>)}
            </tr>
        );
    }
}