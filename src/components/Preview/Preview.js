import React, {Component} from "react";
import TrTable from "./TrTable";
import {confirmAlert} from "react-confirm-alert";
import 'react-confirm-alert/src/react-confirm-alert.css';
import 'react-web-tabs/dist/react-web-tabs.css';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
export default class Preview extends Component{

    constructor(props) {
        //vrati cestu v json souboru
        function getJsonPath(json){
            let path ="";
            for (var key of Object.keys(json)) {
                if (key instanceof Number || !isNaN(Number(key)))
                    return
                else {
                    let s = getJsonPath(json[key])
                    path +=  typeof s !== 'undefined'? key + " " +s : key;
                }
                return path;
            }
        }
        super(props);
        let path = getJsonPath(this.props.json);
        let pathArray = typeof path !== 'undefined'? path.split(" "):[]
        let val = pathArray.reduce((o, n) => o[n], this.props.json);
        let keys = Object.keys(val[0])
        this.state = {
            option:"table",
            json :this.props.json,
            jsonPath: path,
            jsonRoot : val,
            jsonProperties : keys,
            jsonPropertiesWithType : keys.map(item => <li className="table-row" key={item}>
                <div className="col col-1" data-label="Column name">{item}</div>
                <div className="col col-2" data-label="Column type">{this.typeOf(val[0][item])}</div>
                <div className="col col-3" data-label="Column action"> <button className="table-button" value={item} onClick={this.submitDelete}>Delete</button>
                </div>
                </li>),
            columnsHead : keys.map(item => <th key={item}> {item} </th>)
        };
    }
    //vrati typ sloupce
    typeOf = (columnValue) => {
        if (columnValue !== null && columnValue !== undefined) {
            if (typeof columnValue === 'boolean' || ['TRUE', 'FALSE'].includes(columnValue.toString().toUpperCase())) {
                return "Boolean";
            } else if (columnValue instanceof Number||!isNaN(Number(columnValue))) {
                return "Number";
            } else if (!isNaN(new Date(columnValue).getDate())) {
                return "Date";
            }else if (typeof columnValue === 'object'||typeof columnValue === 'object' || columnValue.constructor === Object) {
                return "Object";
            } else {
                return "String";
            }
        }
        return undefined;
    }
    arrayRemove = (arr, value) => {

        return arr.filter(function(element){
            return element !== value;
        });
    }
// smaze sloupec
    submitDelete = (event) =>{
        confirmAlert({
            title: 'Confirm to submit',
            message: 'Are you sure to do this.',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.deleteProperty(event)
                },
                {
                    label: 'No'
                }
            ]
        });
    }

    deleteProperty = async (event) => {
        let newProperty = this.arrayRemove(this.state.jsonProperties, event.target.value);
        this.setState({jsonProperties: newProperty})
        this.setState({
            jsonPropertiesWithType: newProperty.map(item => <li className="table-row" key={item}>
                <div className="col col-1" data-label="Column name">{item}</div>
                <div className="col col-2" data-label="Column type">{this.typeOf(newProperty[0][item])}</div>
                <div className="col col-3" data-label="Column action"> <button className="table-button" value={item} onClick={this.submitDelete}>Delete</button>
                </div>
            </li>)
        });
        this.setState({columnsHead: newProperty.map(item => <th key={item}> {item} </th>)});

    }
    //presmeruje na vyzadovanou stranku
    toChartPage= () =>{
        const properties = this.state.jsonRoot
        const columnsHead = this.state.jsonProperties
        switch (this.state.option){
            case "table":
                this.props.navigation("/table",{state:{properties:properties, columnsHead:columnsHead}})
                break;
            case "line":
                let x = document.getElementById("horizontal_axis").options[document.getElementById("horizontal_axis").selectedIndex].value
                let y = document.getElementById("vertical_axis").options[document.getElementById("vertical_axis").selectedIndex].value
                this.props.navigation("/lineChart",{state:{json:this.state.jsonRoot, x: x, y:y,SceneName:this.props.scene}})
                break;
            case "pie":
                let values = document.getElementById("pie_values").options[document.getElementById("pie_values").selectedIndex].value
                let labels = document.getElementById("pie_labels").options[document.getElementById("pie_labels").selectedIndex].value
                this.props.navigation("/pieChart",{state:{json:this.state.jsonRoot, values: values, labels:labels,SceneName:this.props.scene}})
                break;
        }
    }
    changedTabs = (tabOption) => {
        this.setState( {option: tabOption})
    }
    render() {
        console.log("render() method");
        return (
            <div className="main">
                <main>
                    <div>Scene preview</div>
                    <div className="description"><h1>
                        Scene name: {this.props.scene}
                    </h1>
                        <h2>Scene description: {this.props.description}</h2>
                    </div>
                <div>
                    <textarea  id="json-preview" readOnly defaultValue = {JSON.stringify(this.props.json)}/>
                </div>
                    <div className="description">
                        <h2>
                            This website allows you to visualize json data in 3 forms: tables, line charts and pie charts.
                            <br></br> Click on the one that suits you and start configuring.
                    </h2>
                    </div>
                <Tabs
                    defaultTab="table"
                    onChange={(tabId) => { this.changedTabs(tabId) }}
                >
                    <TabList>
                        <Tab tabFor="table">Table</Tab>
                        <Tab tabFor="line">Line chart</Tab>
                        <Tab tabFor="pie">Pie chart</Tab>
                    </TabList>
                    <TabPanel tabId="table">
                        <h2>Configure your table</h2>
                        <div id="json-properties-type" >
                            <ul className="responsive-table">
                                <li className="table-header">
                                    <div className="col col-1">Column name</div>
                                    <div className="col col-2">Column type</div>
                                    <div className="col col-3">Action</div>
                                </li>
                                {this.state.jsonPropertiesWithType}
                            </ul>
                        </div>
                    </TabPanel>
                    <TabPanel tabId="line">
                        <h2>Configure your line chart</h2>
                        <h3>A line chart is a graphical representation of an asset's
                            historical price action that connects a series
                            of data points with a continuous line. </h3>
                        <div className="selectdiv">
                            <label className="charts-configure-label" htmlFor="horizontal_axis">
                                Choose a Horizontal axis (x-axis):
                            </label>
                            <select name="horizontal_axis" id="horizontal_axis" >
                                {this.state.jsonProperties.map(item => <option  value={item} key={item}>{item}</option>)}
                            </select>
                        </div>
                        <div className="selectdiv">
                            <label  className="charts-configure-label" htmlFor="vertical_axis">
                                Choose a Vertical axis (x-axis):
                            </label>
                            <select name="vertical_axis" id="vertical_axis" >
                                {this.state.jsonProperties.map(item => <option  value={item} key={item}>{item}</option>)}
                            </select>
                        </div>
                    </TabPanel>
                    <TabPanel tabId="pie">
                        <h2>Configure your pie chart</h2>
                        <h3>Pie chart is circle divided to slices.
                            Each slice represents a numerical value and has
                            slice size proportional to the value.</h3>
                        <div className="selectdiv">
                            <label className="charts-configure-label" htmlFor="pie_values">
                                Choose a Data values:
                            </label>
                            <select name="pie_values" id="pie_values" >
                                {this.state.jsonProperties.map(item => <option  value={item} key={item}>{item}</option>)}
                            </select>
                        </div>
                        <div className="selectdiv">
                            <label className="charts-configure-label" htmlFor="pie_labels">
                                Choose a Data labels:
                            </label>
                            <select name="pie_labels" id="pie_labels" >
                                {this.state.jsonProperties.map(item => <option  value={item} key={item}>{item}</option>)}
                            </select>
                        </div>
                    </TabPanel>
                </Tabs>
                <div>
                    <button onClick={this.toChartPage}>Save</button>
                </div>
                </main>
                <footer id="footer">
                    <div>
                        <div className="credits">Designed by Fedorova Daria for KAJ semestral project</div>
                    </div>
                </footer>
            </div>
        )
    }
}