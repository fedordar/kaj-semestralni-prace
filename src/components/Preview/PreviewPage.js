import {useLocation} from "react-router-dom"
import React from "react";
import Preview from "./Preview";
import {useNavigate} from "react-router-dom";

export const PreviewPage = () => {
    const location = useLocation();
    const ooo = useNavigate();
    return (
        <Preview  navigation={ooo} scene={location.state.scene} description={location.state.description} json={location.state.json}/>
    )
}