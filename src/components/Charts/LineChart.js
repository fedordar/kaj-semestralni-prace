import React from 'react';
import Plot from '../../../node_modules/react-plotly.js/react-plotly';
import {useLocation} from "react-router-dom";

export const LineChart = () => {
    const location = useLocation();

    // Change data structure
    function transformData (data) {
        let plot_data = [];

        let x = [];
        let y = [];
        data.map(each => {
            x.push(each[location.state.x])
            y.push(each[location.state.y])
        })
        plot_data['x'] = x;
        plot_data['y'] = y;

        console.log(plot_data)

        return plot_data
    }
    {
        return (
            <div class="main">
                <main>
                    <div>Line chart result</div>
                <Plot
                    data = {[
                        {type: 'scatter',
                            mode: 'lines',
                            x: transformData(location.state.json)['x'],
                            y: transformData(location.state.json)['y'],
                            marker: { color: '#ed022d'}}
                    ]}
                    layout = { {width: 1000, height: 500, title: location.state.SceneName} }
                />
                </main>
                <footer id="footer">
                    <div>
                        <div className="credits">Designed by Fedorova Daria for KAJ semestral project</div>
                    </div>
                </footer>
            </div>
        );
    }
}