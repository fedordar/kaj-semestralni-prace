import React from 'react';
import Plot from '../../../node_modules/react-plotly.js/react-plotly';
import {useLocation} from "react-router-dom";

export const PieChart = () => {
    const location = useLocation();

    // Change data structure
    function transformData (data) {
        let plot_data = [];

        let values = [];
        let labels = [];
        data.map(each => {
            values.push(each[location.state.values])
            labels.push(each[location.state.labels])
        })
        plot_data['values'] = values;
        plot_data['labels'] = labels;

        console.log(plot_data)

        return plot_data
    }
        return (
            <div className="main">
            <main>
            <div>Pie chart result</div>
                <Plot
                    data = {[
                        {type: 'pie',
                            mode: 'lines',
                            values: transformData(location.state.json)['values'],
                            labels: transformData(location.state.json)['labels'],
                            marker: { color: '#ed022d'}}
                    ]}
                    layout = { {width: 1000, height: 500, title: location.state.SceneName} }
                />
            </main>
                <footer id="footer">
                    <div>
                        <div className="credits">Designed by Fedorova Daria for KAJ semestral project</div>
                    </div>
                </footer>
            </div>

        );
}